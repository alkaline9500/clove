# Contributions

## Nic Manoogian (njm7461)

- User login
- User registration
- Session creation and management
- Queuing / Dequeuing
- Media
- Routing web service
- Poster Design

## Nick Depinet (nld9806)
- Tutoring Shift Model Implementation
- Tutoring Shift Creation & Management Endpoints
- Other Api Endpoint work
- Documentation work

## Michael Plante (mgp9795)
- ER Model reduction
- Media (Old, disused versions)
- Parts of proposal document