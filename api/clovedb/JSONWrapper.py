class JSONWrapper():
    """
    An encapsulation of a json object, allowing for json_object export
    """
    def __init__(self, json_data):
        self.json_data = json_data

    def json_object(self):
        return self.json_data
