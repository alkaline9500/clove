from .BaseModel import BaseModel


class Serializable(BaseModel):
    """
    An object that can be represented as a json object
    """

    def json_object(self):
        return None
