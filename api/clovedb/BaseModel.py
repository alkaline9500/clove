import clovedb
from peewee import *
from .Response import *


class BaseModel(Model):
    """
    The BaseModel is an extension of the Model, adding the default database
    and identification lookups.
    """
    @classmethod
    def for_id(cls, id_):
        try:
            item = cls.get(id=id_)
            return Response(data=item)
        except cls.DoesNotExist:
            not_found_message = cls.__name__ + " with specified ID not found"
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description=not_found_message)

    def clear(self):
        """
        Removes an object from the database
        """
        self.delete_instance(recursive=True)
        return Response(description="Successfully deleted.")

    class Meta():
        database = clovedb.database
