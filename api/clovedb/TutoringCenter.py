from peewee import *
from .Serializable import Serializable
from .Response import *
from .ListJSONCollector import ListJSONCollector

class TutoringCenter(Serializable):
    """
    A tutoring center with a unique name and non-unique location.
    """
    _name = CharField()
    _location = CharField()

    @classmethod
    def add(cls, name, location):
        """
        Validates name and location and adds the new center to the database.
        Returns a Response object
        """

        # Strip whitespace
        if name:
            name = name.strip()
        if location:
            location = location.strip()

        # Name
        if not name:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The tutoring center must have a " +
                            "name.")
        # Location
        if not location:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The tutoring center must have a " +
                            "location.")

        name_query = TutoringCenter.select().where(TutoringCenter._name == name)

        if name_query.exists():
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="A tutoring center with that name " +
                            "already exists. Please choose another.")

        try:
            new_center = TutoringCenter.create(_name=name, _location=location)
            return Response(data=new_center)
        except DatabaseError:
            return Response(successful=False, status_code=HTTP_INTERNAL_ERROR,
                            description="We couldn't add that center, but it " +
                                        "wasn't your fault. Please try again.")

    def modify(self, name=None, location=None):
        """
        Validates and modifies a tutoring center.
        Returns a response object for success/failure.
        """

        if location:
            self._location = location.strip()

        if name:
            name = name.strip()

        if name:
            name_query = self.select().where(
                self.__class__._name == name)
            if name_query.exists():
                return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                                description="A center with that name " +
                                            "already exists. You'll need to" +
                                            "choose another.")
            self._name = name


        self.save()
        return Response(description="Updated successfully.")

    @classmethod
    def search(cls, name=None, location=None):
        """
        search returns a Reponse (success) with a query containing all tutoring
        center objects matching the name and location fields
        """

        query = TutoringCenter.select()

        if name:
            name = name.strip()
            query = query.where(TutoringCenter._name == name)
        if location:
            location = location.strip()
            query = query.where(TutoringCenter._location == location)

        return Response(data=ListJSONCollector([u for u in query]))

    @classmethod
    def find(cls, username=None, email=None):
        """
        find is a hard query that returns a Response if any only if there is a
        match against the provided fields
        """

        query = TutoringCenter.select()

        if name:
            name = name.strip()
            query = query.where(TutoringCenter._name == name)
        if location:
            location = location.strip()
            query = query.where(TutoringCenter._location == location)

        if query.count() != 1:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="There are no tutoring centers " +
                                        "matching that request.")
        return Response(data=query.first())

    def json_object(self):
        return {
            "name": self._name,
            "location": self._location,
            "id": self.id
        }
