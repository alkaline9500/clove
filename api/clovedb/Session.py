from peewee import *
from .Serializable import Serializable
from .User import User
from .Response import *
from .TutoringCenter import TutoringCenter
from .ListJSONCollector import ListJSONCollector
from .JSONWrapper import JSONWrapper
from datetime import datetime, date, timedelta
from .utils import in_unix

# Workaround constant as DateTimeFields cannot be nulled
NULL_DATETIME = datetime(1,1,1,1,1)

class SessionStatus():
    """
    Status of the session
    """
    requested = 1
    in_progress = 2
    completed = 3

class Session(Serializable):
    """
    A session held at a tutoring center with a tutor
    """
    _student = ForeignKeyField(User, related_name='_students', null=False)
    _center = ForeignKeyField(TutoringCenter, related_name='_centers', null=False)
    _tutor = ForeignKeyField(User, related_name='_tutors', null=True)
    _topic = TextField()
    _requested_time = DateTimeField()
    _start_time = DateTimeField()
    _end_time = DateTimeField()


    @classmethod
    def add(cls, student, center=None, tutor=None, topic=None):
        """
        Validates a new session request and adds it to the database.
        Returns a Response object
        """
        if not center:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="Session request must have a center.")
        try:
            center = TutoringCenter.get(id=center)
        except TutoringCenter.DoesNotExist:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The center with that ID does not" +
                                        " exist.")
        if not topic:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="Session request must have a topic.")

        created = datetime.utcnow()

        created_session = Session.create(_student=student, _center=center,
                                         _tutor=None, _topic=topic,
                                         _requested_time=created,
                                         _start_time=NULL_DATETIME, _end_time=NULL_DATETIME)


        return Response(data=created_session)

    @classmethod
    def search(cls, student=None, center=None, tutor=None, topic=None):
        query = cls.select()
        if student:
            query = query.where(cls._student == student)
        if tutor:
            query = query.where(cls._tutor == tutor)
        if center:
            query = query.where(cls._center == center)
        if topic:
            query = query.where(cls._topic == topic)

        query = query.order_by(cls._requested_time.desc())

        return Response(data=ListJSONCollector(query))

    @classmethod
    def json_queue(cls, center=None):
        if not center:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="You must specify a center.")
        try:
            center = TutoringCenter.get(id=center)
        except TutoringCenter.DoesNotExist:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The center with that ID does not" +
                                        " exist.")
        query = cls.select().where((cls._center == center) &
                                   (cls._start_time == NULL_DATETIME))
        return Response(data=ListJSONCollector([q for q in query]))

    @classmethod
    def begin_session(cls, session=None, tutor=None):
        """
        Assigns a tutor to a session sets the start time
        """
        if not tutor:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="You must specify a tutor.")
        if not session:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="You must specify a session.")
        try:
            tutor = User.get(id=tutor)
        except User.DoesNotExist:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The user with that ID does not" +
                                        " exist.")
        try:
            session = Session.get(id=session)
        except Session.DoesNotExist:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The session with that ID does not" +
                                        " exist.")

        if session.status() != SessionStatus.requested:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The specified session is already " +
                            "started")
        session._tutor = tutor
        session._start_time = datetime.utcnow()
        session.save()
        return Response(data=session)

    @classmethod
    def end_session(cls, session=None):
        """
        Closes the session by setting the end time
        """
        if not session:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="You must specify a session.")
        try:
            session = Session.get(id=session)
        except Session.DoesNotExist:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The session with that ID does not" +
                                        " exist.")

        # Sessions can be closed before they actually begin, as is the case
        # when a student solves a problem on their own.

        if session.status() == SessionStatus.completed:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The specified session is already " +
                            "completed.")

        session._end_time = datetime.utcnow()
        session.save()
        return Response(data=session)


    def status(self):
        """
        Returns the current SessionStatus of the Session
        """
        status = SessionStatus.requested
        if self._start_time != NULL_DATETIME and self._tutor:
            status = SessionStatus.in_progress
        if self._end_time != NULL_DATETIME:
            status = SessionStatus.completed
        return status


    def json_object(self):
        json_dictionary = {
            "id": self.id,
            "student": self._student.json_object(),
            "center": self._center.json_object(),
            "topic": self._topic,
        }

        status = SessionStatus.requested

        if self._requested_time:
            json_dictionary["requested_time"] = in_unix(self._requested_time)
        if self._start_time != NULL_DATETIME:
            json_dictionary["start_time"] = in_unix(self._start_time)
        if self._end_time != NULL_DATETIME:
            json_dictionary["end_time"] = in_unix(self._end_time)
        if self._tutor:
            json_dictionary["tutor"] = self._tutor.json_object()

        json_dictionary["media"] = [m._url for m in self._media]

        json_dictionary["status"] = self.status()

        return json_dictionary
