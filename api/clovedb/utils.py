import calendar


def in_unix(input_):
    """
    Allows for cleaner timestamping
    """
    return calendar.timegm(input_.utctimetuple())
