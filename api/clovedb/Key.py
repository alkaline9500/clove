from peewee import *
from .Serializable import Serializable
from .Response import *
import bcrypt
import hashlib


class Key(Serializable):
    """
    An abstract class that represents a unique key object
    """
    _content = CharField(unique=True)

    @classmethod
    def generate(cls):
        key_content = None
        while not (key_content or
                   cls.select().where(cls._content == key_content).exists()):
            salt = bcrypt.gensalt().encode()
            key_content = hashlib.sha256(salt).hexdigest()
        return key_content

    @classmethod
    def find(cls, string):
        invalid_credentials_response = Response(successful=False,
                                                status_code=HTTP_UNAUTHORIZED,
                                                description="That key string " +
                                                            "is invalid.")
        if not string:
            return invalid_credentials_response
        try:
            token = cls.get(_content=string)
            return Response(data=token)
        except cls.DoesNotExist:
            return invalid_credentials_response

    @classmethod
    def is_valid(cls, key_string):
        return cls.find(key_string).successful
