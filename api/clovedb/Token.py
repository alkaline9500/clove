from peewee import *
from .Key import Key
from .Response import *
from .User import User
from datetime import datetime, timedelta
import bcrypt

# Deltas
EXPIRATION_DELTA = 3600


class Token(Key):
    """
    The JSON Web Token object. One user can have many tokens. Each token must
    expire at some point in the future.
    """
    _user = ForeignKeyField(User, related_name='tokens', null=False)
    _expiration = DateTimeField()

    @classmethod
    def authenticate(cls, username=None, password=None):
        """
        authenticate allows users to authenticate with either a username or a
        password.
        Returns a new token, generated from the username and timestamp
        """
        invalid_credentials_response = Response(successful=False,
                                                status_code=HTTP_UNAUTHORIZED,
                                                description="Failed to " +
                                                            "authenticate user")

        if not password or not username:
            return invalid_credentials_response

        try:
            user = User.get(_username=username)

            if not user or bcrypt.hashpw(password,
                                         user._password) != user._password:
                return invalid_credentials_response

            token = Token.create(_content=cls.generate(),
                                 _user=user,
                                 _expiration=datetime.utcnow() + timedelta(
                                     seconds=EXPIRATION_DELTA))

            return Response(data=token)
        except (User.DoesNotExist, cls.DoesNotExist):
            return invalid_credentials_response

    @classmethod
    def cleanup(cls):
        """
        cleanup will remove all expired tokens from the database
        This function is called by authenticate.
        """
        delete_query = cls.delete().where(
            Token._expiration < datetime.utcnow())
        delete_query.execute()

    def extend(self):
        """
        extend will add EXPIRATION_DELTA to the expiration date of
        the specified token
        """
        try:
            self._expiration = datetime.utcnow() + timedelta(
                seconds=EXPIRATION_DELTA)
            self.save()
            return Response(data=self)
        except Token.DoesNotExist:
            return Response(successful=False, status_code=HTTP_INTERNAL_ERROR,
                            description="Could not extend token life.")

    def json_object(self):
        return {
            "token": self._content,
            "user": self._user.json_object()
        }
