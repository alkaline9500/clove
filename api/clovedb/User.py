from peewee import *
from .Serializable import Serializable
from .Response import *
from re import match
import bcrypt
from .ListJSONCollector import ListJSONCollector

# Enforcements
PASSWORD_MIN = 6
USERNAME_MIN = 4


class User(Serializable):
    """
    A basic user object containing profile information and credentials
    """
    _first_name = CharField()
    _last_name = CharField()
    _username = CharField(unique=True)
    _password = CharField()
    _email = CharField(unique=True)

    @classmethod
    def add(cls, first_name=None, last_name=None, username=None, password=None,
            email=None):
        """
        add validates all user data and adds the user in to the system
        Returns a Response object
        """

        # Strip whitespace
        if first_name:
            first_name = first_name.strip()
        if last_name:
            last_name = last_name.strip()
        if username:
            username = username.strip()
        if email:
            email = email.strip()

        # Names
        if not first_name or not last_name:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="You must provide a first and last " +
                                        "name.")
        # Credentials
        if not username or not password:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="You must have a username and password")
        if len(username) < USERNAME_MIN:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="Username must be longer than " + str(
                                USERNAME_MIN) + " characters.")
        # Email was provided
        if not email:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="You must provide an email.")
        # Email matches regex
        regex = "^[a-zA-Z0-9._%-+]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$"
        if not match(regex, email):
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="Please check the formatting of " +
                                        "your email.")
        # Password length
        if len(password) < PASSWORD_MIN:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="Password minimum length is " + str(
                                PASSWORD_MIN))

        username_query = User.select().where(User._username == username)
        email_query = User.select().where(User._email == email)

        if username_query.exists():
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="A user with that username already " +
                                        "exists. You'll need to choose " +
                                        "another.")
        if email_query.exists():
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="A user with that email already " +
                                        "exists. You'll need to choose " +
                                        "another.")

        hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())

        try:
            user = User.create(_first_name=first_name, _last_name=last_name,
                               _username=username,
                               _password=hashed_password, _email=email)
            return Response(data=user)
        except DatabaseError:
            return Response(successful=False, status_code=HTTP_INTERNAL_ERROR,
                            description="We couldn't add that user, but it " +
                                        "wasn't your fault. Please try again.")

    def modify(self, first_name=None, last_name=None, username=None,
               password=None, email=None):
        """
        modify updates a user based on an auth token. Any fields given in the
        optionals will be changed. Unique fields will be verified before they
        are saved.
        Returns a response object for success/failure.
        """

        if first_name:
            self._first_name = first_name.strip()

        if last_name:
            self._last_name = last_name.strip()

        if username:
            username_query = self.select().where(
                self.__class__._username == username)
            if username_query.exists():
                return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                                description="A user with that username " +
                                            "already exists. You'll need to" +
                                            "choose another.")
            self._username = username.strip()

        if email:
            email_query = User.select().where(User._email == email)
            if email_query.exists():
                return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                                description="A user with that email already " +
                                            "exists. You'll need to " +
                                            "choose another.")
            self._email = email.strip()
        if password:
            self._password = password

        self.save()
        return Response(description="Updated successfully")

    @classmethod
    def search(cls, first_name=None, last_name=None, username=None, email=None):
        """
        search returns a Reponse (success) with a query containing all user
        objects matching the first_name and last_name fields
        """

        query = User.select()

        if username:
            username = username.strip()
            query = query.where(User._username == username)
        if email:
            email = email.strip()
            query = query.where(User._email == email)
        if first_name:
            first_name = first_name.strip()
            query = query.where(User._first_name == first_name)
        if last_name:
            last_name = last_name.strip()
            query = query.where(User._last_name == last_name)

        return Response(data=ListJSONCollector([u for u in query]))

    @classmethod
    def find(cls, username=None, email=None):
        """
        find is a hard query that returns a Response if any only if there is a
        match against the provided fields
        """
        query = User.select()

        if username:
            username = username.strip()
            query = query.where(User._username == username)
        if email:
            email = email.strip()
            query = query.where(User._email == email)

        if query.count() != 1:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="There are no users " +
                                        "matching that request.")
        return Response(data=query.first())

    def json_object(self):
        return {
            "first_name": self._first_name,
            "last_name": self._last_name,
            "email": self._email,
            "username": self._username,
            "id": self.id
        }

    def full_name(self):
        return " ".join([self._first_name, self._last_name])

    def response(self):
        """
        The response for the /me endpoint.
        :return: a Response object with this User inside.
        """
        return Response(data=self)
