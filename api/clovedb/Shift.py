from peewee import *
from .Serializable import Serializable
from .User import User
from .Response import *
from .TutoringCenter import TutoringCenter
from .ListJSONCollector import ListJSONCollector
from .JSONWrapper import JSONWrapper
from datetime import datetime, date, timedelta
from .utils import in_unix

class Shift(Serializable):
    """
    A Tutor's shifts held at a tutoring center
    Currently each individual shift is it's own entry in the db,
    so monday this week and monday next week are different shifts
    """
    _tutor = ForeignKeyField(User)
    _center = ForeignKeyField(TutoringCenter)
    _start_time = DateTimeField()
    _end_time = DateTimeField()


    @classmethod
    def add(cls, tutor=None, center=None, start_time=None, end_time=None):
        """
        Validates a new shift request and adds it to the database.
        Returns a Response object
        """

        if not tutor:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="A shift must have a tutor working it.")

        if not start_time:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="A shift must have a start time.")

        if not end_time:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="A shift must have an end time.")
        if end_time - start_time < 0:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="A shift's end time must be after " +
                                        "it's start time.")

        try:
            center = TutoringCenter.get(id=center)
        except TutoringCenter.DoesNotExist:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The center with that ID does not" +
                                        " exist.")

        created_shift = Shift.create(_center=center, _tutor=tutor,
                                         _start_time=start_time, _end_time=end_time)

        return Response(data=created_shift)

    """
    Search for shifts
    Parameters:
        tutor: the tutor hosting the shift
        center: the center the shift is at
        start_time: the start time for the shift
        end_time: the end_time for the shift
        current: whether or not the shift is currently active
    """
    @classmethod
    def search(cls, tutor=None, center=None, start_time=None, end_time=None, current=False):
        query = cls.select()
        if tutor:
            query = query.where(cls._tutor == tutor)
        if center:
            query = query.where(cls._center == center)
        if start_time:
            query = query.where(cls._start_time >= start_time)
        if end_time:
            query = query.where(cls._end_time <= end_time)
        if current:
            curTime = in_unix(datetime.utcnow())
            # Since each shift is it's own entry, we can easily determine if a shift is active
            query = query.where((cls._start_time <= curTime) & (cls._end_time >= curTime))

        return Response(data=ListJSONCollector(query))

    """
    Convert the shift to a json object to return
    """
    def json_object(self):
        json_dictionary = {
            "id": self.id,
            "tutor": self._tutor.json_object(),
            "center": self._center.json_object(),
            "start_time": self._start_time,
            "end_time": self._end_time
        }

        return json_dictionary
