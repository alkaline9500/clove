from peewee import *
from .Serializable import Serializable
from .Session import Session
from .Response import *
from .ListJSONCollector import ListJSONCollector

class Media(Serializable):
    """
    Media object contains photos taken during a session
    """
    _url = TextField()
    _session = ForeignKeyField(Session, related_name='_media')

    @classmethod
    def add(cls, url=None, session=None):
        """
        Adds a image to the database
        """
        if not url:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="Media must have a URL.")
        if not session:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="Media must have a session.")

        try:
            session = Session.get(id=session)
        except Session.DoesNotExist:
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                            description="The session with that ID does not" +
                                        " exist.")

        media_query = Media.select().where(Media._url == url, Media._session == session)

        if media_query.exists():
            return Response(successful=False, status_code=HTTP_BAD_REQUEST,
                        description="Media with that URL already exists " +
                                    "for that session.")


        try:
            media = Media.create(_url=url, _session=session)
            return Response(data=media)
        except DatabaseError:
            return Response(successful=False, status_code=HTTP_INTERNAL_ERROR, description=
                            "Something went wrong when adding the image. Please try again.")

    @classmethod
    def search(cls, media_id=None, session_id=None):
        """
        Searches the database for a particular Media item
        """
        query = cls.select()
        if media_id:
            query = query.where(cls._id == media_id)
        if image:
            query = query.where(cls._session == session_id)
        return Response(data=ListJSONCollector([u for u in query]))


    def json_object(self):
        return {
            'url' : self._url,
            'session' : self._session.id
        }
