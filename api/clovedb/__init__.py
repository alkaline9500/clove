from peewee import *

"""
clovedb
Stores all models for the clove database
"""

################
# Constants
################

DATABASE_IDENTIFIER = 'clove'
DATABASE_VERSION = '_test'
DATABASE = DATABASE_IDENTIFIER + DATABASE_VERSION


def init_database(remote_database):
    global database
    if not remote_database:
        database = SqliteDatabase(DATABASE_IDENTIFIER + '.db',
                                  threadlocals=True)
    else:
        database = PostgresqlDatabase(DATABASE,
                                      user=DATABASE_IDENTIFIER,
                                      password=DATABASE_IDENTIFIER,
                                      threadlocals=True)


def create_tables():
    """
    create_tables is an internal function to ensure that all tables in the
    system are created.
    """

    from .User import User
    from .Token import Token
    from .Session import Session
    from .Shift import Shift
    from .TutoringCenter import TutoringCenter
    from .Media import Media

    User.create_table(fail_silently=True)
    Token.create_table(fail_silently=True)
    Session.create_table(fail_silently=True)
    Shift.create_table(fail_silently=True)
    TutoringCenter.create_table(fail_silently=True)
    Media.create_table(fail_silently=True)


def connect(remote_database=False):
    init_database(remote_database)
    database.connect()
    create_tables()
