import requests
import random
import string
import argparse

"""
populate.py
Makes API calls to fill the database
"""

API_URL = "http://localhost:8888/api"

first_names = ["Alicia","Roxanne","Tobie","Anisha","Latanya","Lincoln","Stefania","Gwen","Adalberto","Madaline","Golda","Vonnie","Alica","Eladia","Lissa","Tari","Honey","Lauren","Sana","Donette","Micaela","Rubye","Exie","Deandre","Johana","German","Gerardo","Sharolyn","Hermila","Crysta","Abraham","Terri","Kyle","Enriqueta","Ara","Danilo","Raisa","Cleo","Renate","Lavone","Sherryl","Gustavo","Sophie","Bee","Edra","Susan","Henriette","Chance","Jenny","Rosa","Rick","Salvatore","Robbi","Willie","Marget","Katrice","Sharlene","Mayola","Dwayne","Nestor","Paula","Estrella","Trena","Tyron","Nyla","Merna","Freeda","Sunshine","Drema","Desirae","Abbie","Nelly","Shelby","Melani","Robby","Andree","Alana","Margeret","Alyce","Leonore","Noreen","Danial","Dannette","Danelle","Dorthey","Ismael","Duane","Jacquelin","Ashlyn","Darron","Lynetta","Allegra","Santo","Johnette","Dagny","Ignacia","Tyson","Roxana","Eldora","Johna","Iraida","Brittny","Renate","Brittni","Ahmad","Tonette","Pamala","Nieves","Lynda","Jennie","Russel","Jaymie","Jewel","Earle","Candace","Janell","Lynne","Florencio","Dorsey","Linnea","Laurel","Debrah","Carri","Candelaria","Shana","Raye","Eula","Kyla","Leonia","Major","Jeromy","Denise","Shirlee","Yesenia","Chang","Bea","Cathrine","Kazuko","Corrin","Marleen","Irena","Robby","Edris","Lasonya","Rita","Micki","Florentina","Tressa","Ji","Jenna","Melisa","Hedy","Brenna","Susy","Tari","Mao","Dana","Berniece","Micah","Leeann","Debora","Wendi","Farrah","Sid","Ebonie","Jeri","Abe","Louetta","Lura","Shayna","Jeannette","Herschel","Stephenie","Kenneth","Belia","Ellis","Ludivina","Samuel","Lynelle","Kyoko","Jenny","Ewa","Jefferey","Hilma","Colette","Antonetta","Walton","Candy","Myra","Mica","Emmitt","Porfirio","Grazyna","Ilana","Zena","Rosena","Dwight","Lyman"]

last_names = ["Hagberg","Engel","Borman","Olinger","Lard","Bryant","Neubert","Bevacqua","Degner","Solar","Ferrell","Heber","Wilkerson","Lecroy","Plantz","Brittan","Kimbler","Degnan","Cloyd","Fordham","Lippert","Goodrich","Defrancisco","Carmouche","Broadnax","Ehrman","Klemm","Morano","Schurg","Beaton","Blaylock","Hoxie","Cun","Fitzwater","Culver","Farfan","Wedgeworth","Rake","Chappelle","Whiten","Gable","Neves","Cancel","Claycomb","Mchargue","Tarver","Cousins","Reisinger","Pocock","Shoop","Hadsell","Nystrom","Messersmith","Bazan","Gravel","Straughter","Lundahl","Bustillos","Bladen","Remo","Kehrer","Veilleux","Mayville","Hulen","Skoglund","Chittenden","Harden","Lochner","Kilman","Oda","Blacker","Mathena","Chabolla","Ingwersen","Arens","Milford","Esser","Lefever","Wilkerson","Wahl","Reeve","Rickard","Valone","Bischoff","Lucca","Schlegel","Kramer","Broadhurst","Funari","Wimbley","Stiver","Walberg","Moots","Middlebrooks","Luebbers","Pool","Hauptman","Severt","Riggs","Farah","Lemke","Stenson","Murrieta","Dacanay","Hofer","Moretti","Treece","Huntsberry","Mckinzie","Amarante","Melugin","Quaid","Vibbert","Dery","Solan","Haffey","Dalal","Mcgonagle","Marsh","Reasor","Bartel","Gallop","Dobbin","Cullen","Vanantwerp","Barnhouse","Whittemore","Stutler","Stein","Morello","Schermerhorn","Lipsey","Nevitt","Isabel","Medlen","Lesage","Braun","Young","Mcevoy","Ewers","Roldan","David","Knighton","Thibodeaux","Mariscal","Certain","Lagrange","Morocco","Wirta","Tillison","Philson","Cuellar","Branson","Beerman","Mansfield","Strong","Cameron","Tighe","Heyne","Vanderbilt","Purpura","Canela","Ellingson","Schebler","Beckford","Freese","Cull","Wallace","Rotter","Maxie","Lind","Hanel","Slabaugh","Carlino","Pinheiro","Wolfgang","Umana","Husman","Bobbitt","Hanselman","Dieterich","Watkin","Perry","Packett","Vantassel","Saiz","Wallingford","Dietrick","Shenkel","Giffin","Jaquith","Creason","Pasek","Hartzler","Vargo","Belue","Schroer","Rogers"]

class User:
    """
    A User class to populate the database
    """
    def __init__(self):
        self.first_name = random.choice(first_names)
        self.last_name = random.choice(last_names)
        initials = self.first_name[0] + random.choice(string.ascii_lowercase) + self.last_name[0]
        self.username = initials.lower() + "".join(random.sample(string.digits, 4))
        self.email = self.username + "@rit.edu"
        self.password = "".join(random.sample(string.hexdigits, 8))
        self.api_key = None

    def __init__(self, first_name=None, last_name=None, email=None, username=None, password=None):
        self.first_name = first_name or random.choice(first_names)
        self.last_name = last_name or random.choice(last_names)
        initials = self.first_name[0] + random.choice(string.ascii_lowercase) + self.last_name[0]
        self.username = username or initials.lower() + "".join(random.sample(string.digits, 4))
        self.email = email or self.username + "@rit.edu"
        self.password = password or "".join(random.sample(string.hexdigits, 8))
        self.api_key = None

    def register(self):
        data = {
            'first_name' : self.first_name,
            'last_name' : self.last_name,
            'username' : self.username,
            'password' : self.password,
            'email' : self.email
            }
        return requests.post(API_URL + "/register", data=data)

    def login(self):
        data = {
        	"username" : self.username,
        	"password" : self.password
        }
        response = requests.post(API_URL + "/login", data=data)
        response_json = response.json()
        if "token" in response_json:
            self.api_key = response_json["token"]
        return response

    def create_session(self, center, topic):
        data = {
            'center' : center.id,
            'topic' : topic
        }
        headers = {
            'CloveAuthentication' : self.api_key
        }
        return requests.post(API_URL + "/sessions", data=data, headers=headers)

    def __repr__(self):
        return "User: {f} {l}, {u}, {e}, {p}".format(f=self.first_name, l=self.last_name, u=self.username, e=self.email, p=self.password)

class TutoringCenter:
    """
    A sample tutoring center
    """
    def __init__(self, name, location):
        self.name = name
        self.location = location
        self.id = None

    def register(self, api_key):
        data = {
            'name' : self.name,
            'location' : self.location
        }
        headers = {
            'CloveAuthentication' : api_key
        }
        response = requests.post(API_URL + "/centers", data=data, headers=headers)
        response_json = response.json()
        if "id" in response_json:
            self.id = response_json["id"]
        return response

def print_response(response):
    print("\t{status}: {response}".format(status=response.status_code, response=response.text))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Clove Test: Populate')
    parser.add_argument('-u', metavar="users", type=int, help='the number of users to generate', default=10)
    parser.add_argument('--url', dest="url", metavar="api_root", help="The url for the root of the api. This should *not* include a trailing slash", default=API_URL)
    args = parser.parse_args()

    API_URL = args.url

    print("Registering base users...")
    users = [User("Nic", "Manoogian", "njm7461@rit.edu", "nmanoogian", "datpass1234")]
    for b in users:
        print_response(b.register())


    print("Registering {u} users...".format(u=args.u))
    for _ in range(args.u):
        u = User()
        users.append(u)
        print_response(u.register())

    main_user = User()
    print("Registering main user {user}...".format(user=main_user.username))
    print_response(main_user.register())

    print("Logging in...")
    print_response(main_user.login())

    tutoring_centers = [TutoringCenter("Bates Tutoring Center", "Gosnell"),
                        TutoringCenter("Computer Science Mentoring Center", "Golisano")]
    topics = ["C", "Objective-C", "Java", "C#", "Physics", "Linear Algebra", "Spanish", "French", "Italian", "Database Design", "Children and Youth","Energy","International Development","Special Populations","Criminal Justice","Environment","Poverty","Substance Use","Economics","Health","Security","Sustainability","Education","Human Development"]

    for tc in tutoring_centers:
        print_response(tc.register(main_user.api_key))

    print("Generating sessions...")
    for _ in range(args.u*2):
        u = random.choice(users)
        print_response(u.login())
        center = random.choice(tutoring_centers)
        topic = random.choice(topics)
        print_response(u.create_session(center, topic))
