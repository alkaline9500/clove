from flask import Flask, request, Response
import argparse
import clovedb
from datetime import datetime

"""
webapi
Flask-based route handler
Bridges requests to the database
"""

# ############
# Utilities #
# ############


def body_from_request(request):
    if request.method == "GET" or request.method == "DELETE":
        body = request.args
    else:
        body = request.get_json(force=True, silent=True)
        if body is None:
            body = request.form
    return body

# ################
# API Endpoints #
# ################

app = Flask(__name__)

BASE_URL = "/api"
HTTP_UNAUTHORIZED = 403
AUTHENTICATION_HEADER = "CloveAuthentication"


@app.before_request
def setup_request():
    request.body = body_from_request(request)

    if request.endpoint == 'login' or request.endpoint == 'register':
        return None  # let the user log in.

    token_string = request.headers.get(AUTHENTICATION_HEADER)
    token_response = Token.find(token_string)
    if not token_response.successful:
        return token_response.to_flask()

    request.token = token_response.data
    request.token.extend()
    request.user = request.token._user

"""
User login endpoint
Post Body:
"""
@app.route(BASE_URL + "/login", methods=['POST'])
def login():
    username = request.body.get('username')
    password = request.body.get('password')

    response = Token.authenticate(username=username,
                                  password=password)
    return response.to_flask()


"""
User registration endpoint
"""
@app.route(BASE_URL + "/register", methods=['POST'])
def register():
    first_name = request.body.get('first_name')
    last_name = request.body.get('last_name')
    username = request.body.get('username')
    password = request.body.get('password')
    email = request.body.get('email')

    response = User.add(first_name=first_name, last_name=last_name,
                        username=username, password=password, email=email)
    return response.to_flask()

"""
User logout endpoint
"""
@app.route(BASE_URL + "/logout", methods=['POST'])
def logout():
    response = request.token.clear()
    return response.to_flask()

"""
Search users by values
"""
@app.route(BASE_URL + "/users", methods=['PUT'])
def users():
    username = request.body.get("username")
    first_name = request.body.get("first_name")
    last_name = request.body.get("last_name")
    email = request.body.get("email")

    user_response = User.search(username=username, email=email,
                                first_name=first_name, last_name=last_name)
    return user_response.to_flask()

"""
Return the user object with the specific id
"""
@app.route(BASE_URL + "/users/<int:id>", methods=['GET'])
def user(id):
    response = User.for_id(id)
    return response.to_flask()

"""
Return the session object with the specific id
"""
@app.route(BASE_URL + "/sessions/<int:id>", methods=['GET'])
def find_session(id):
    response = Session.for_id(id)
    return response.to_flask()

"""
Search for tutoring centers or add a new one
POST request: Create new tutoring center
PUT request: Search for a tutoring center
"""
@app.route(BASE_URL + "/centers", methods=['POST', 'PUT'])
def centers():
    name = request.body.get('name')
    location = request.body.get('location')
    if request.method == 'POST':
        response = TutoringCenter.add(name=name, location=location)
        return response.to_flask()
    elif request.method == 'PUT':
        response = TutoringCenter.search(name=name, location=location)
        return response.to_flask()

"""
Search for tutoring sessions or add a new one
POST request: Add a new tutoring center
PUT request: Search for a tutoring center
"""
@app.route(BASE_URL + "/sessions", methods=['PUT', 'POST'])
def sessions():
    center = request.body.get("center")
    topic = request.body.get("topic")

    if request.method == 'POST':
        response = Session.add(student=request.user, center=center,
                               topic=topic)
        return response.to_flask()
    elif request.method == 'PUT':
        response = Session.search(center=center, topic=topic, student=request.user)
        return response.to_flask()

"""
Return the current queue at the specific tutoring center
"""
@app.route(BASE_URL + "/queue/<int:centerid>", methods=['GET'])
def queue(centerid):
    response = Session.json_queue(center=centerid)
    return response.to_flask()

"""
Begin the specific tutoring session
"""
@app.route(BASE_URL + "/sessions/<int:session_id>/begin", methods=['POST'])
def begin_session(session_id):
    tutor = request.body.get("tutor")
    response = Session.begin_session(session=session_id, tutor=tutor)
    return response.to_flask()

"""
End the specific tutoring session
"""
@app.route(BASE_URL + "/sessions/<int:session_id>/end", methods=['POST'])
def end_session(session_id):
    response = Session.end_session(session=session_id)
    return response.to_flask()

"""
Search for tutor's shifts or add a new one
POST request: Create a new shift
PUT request: Search for a shift
"""
@app.route(BASE_URL + "/shifts", methods=['PUT', 'POST'])
def tutor_shifts():
    center = request.body.get("center")
    start_time = request.body.get("start_time")
    end_time = request.body.get("end_time")
    tutorid = request.body.get("tutor")

    if request.method == 'POST':
        response = Shift.add(tutor=tutorid, center=center,
                            start_time=start_time, end_time=end_time)
        return response.to_flask()
    elif request.method == 'PUT':
        response = Shift.search(tutor=tutorid, center=center,
                                start_time=start_time, end_time=end_time)
        return response.to_flask()

"""
Return the currently active tutoring shifts at a given center
"""
@app.route(BASE_URL + "/currentshifts/<int:center_id>", methods=['GET'])
def current_shifts(center_id):
    response = Shift.search(center=center_id, current=True)
    return response.to_flask()

"""
Add media to the database
"""
@app.route(BASE_URL + "/media", methods=['POST'])
def media():
    session = request.body.get("session")
    url = request.body.get("url")
    response = Media.add(url=url, session=session)
    return response.to_flask()

"""
Main method, run the program
"""
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Clove API Engine")
    parser.add_argument('-s', '--use-ssl',
                        help='Enable SSL with certificate.crt and' +
                             'private_key.key',
                        action="store_true")
    parser.add_argument('-t', '--test',
                        help='Enable test mode',
                        action="store_true")

    parser.add_argument('-r', '--remote',
                        help="Use Postgres remote",
                        action="store_true")

    args = parser.parse_args()
    app.debug = args.test

    clovedb.connect(remote_database=args.remote)

    from clovedb.User import User
    from clovedb.Token import Token
    from clovedb.TutoringCenter import TutoringCenter
    from clovedb.Session import Session
    from clovedb.Media import Media
    from clovedb.Shift import Shift

    context = None
    if args.use_ssl:
        from OpenSSL import SSL

        context = SSL.Context(SSL.SSLv23_METHOD)
        context.use_privatekey_file('private_key.key')
        context.use_certificate_file('certificate.crt')

    app.run(port=8888, ssl_context=context)
