# Reduced Tables

```
User(ID(key), fullName, username, passwordHash, email)
Session(ID(key), topic, status, tutorID, studentID, centerID)
Media(ID(key), photo)
TakenIn(sessionID, photoID(both form key))
Center(ID(key), name, location)
Shift(ID(key), start, end, tutorID, centerID)
```

# SQL Create Statements

```sql
create table User(FullName, varchar(?), username unique, varchar(?), passwordHash, char(hashLength), email, varchar(?), ID, int,
	primary key(ID));

create table Media(ID, int, Photo, TYPE?? primary key(ID));

create table Center(ID, int, name, varchar(?), location, varchar(?), primary key(ID));

create table Shift(ID, int, start, DateTime, end, DateTime, tutorID, int, centerID, int
	primary key(ID), foreign key(tutorID references User(ID), foreign key(centerID) references Center(ID));

create table Session(ID, int, topic, varchar(?), status, int, tutorID, int, studentID, int, centerID, int
	primary key(ID), foreign key(studentID) references User(ID), foreign key(centerID) references Center(ID),
	foreign key(tutorID) references User(ID));

create table TakenIn(sessionID, int, photoID, int primary key(sessionID, photoID)
	foreign key(sessionID) references Session(ID), foreign key(photoID) references Media(ID));
```
