# Main Entities and Relationships

## Entities

### User (Entity)

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Full Name | Char                       |
| Username  | Char / Unique              |
| Password  | Char (hashed)              |
| Email     | Char                       |

### Session (Entity)

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Topic     | Char                       |
| Status    | Integer / Enumeration      |

### Shift (Entity)

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Start     | Date Time                  |
| End       | Date Time                  |

### Tutoring Center (Entity)

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Name      | Char                       |
| Location  | Char                       |

### Photo

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Photo     | Raw Data                   |

## Relationships

### IsTutored

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Student   | Foreign Key (User)         |
| Session   | Foreign Key (Session)      |


### TutorsIn

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Tutor     | Foreign Key (User)         |
| Session   | Foreign Key (Session)      |

### OccursIn

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Center    | Foreign Key (Center)       |
| Session   | Foreign Key (Session)      |

### Works

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Student   | Foreign Key (User)         |
| Shift     | Foreign Key (Shift)        |

### WorkedIn

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Center    | Foreign Key (Center)       |
| Shift     | Foreign Key (Shift)        |

### TakenIn

| Attribute | Type                       |
| ------    | -------                    |
| ID        | ID                         |
| Photo     | Foreign Key (Photo)        |
| Session   | Foreign Key (Session)      |
