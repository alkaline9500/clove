# Clove: A Tutoring System

## Problem

Many of the tutoring centers at the Rochester Institute of Technology are quite successful. They provide space for students to get help with assignments and learn more outside of the lecture hall. These centers are so successful, in fact, that they are often quite crowded.

When a center becomes crowded, students have to wait to the help they need. Because there's typically no take-a-number system for these students, queuing is not strictly enforced (if it's enforced at all) and it may take an hour for a student to get time with a tutor. Without a structured system, a student might sit and wait while a small, vocal minority of students receives the bulk of the attention from tutors.

## Solution

Clove is a queueing system designed to solve this issue. Students can use clove to enter a queue to ensure they are able to meet with a tutor and get the help they need. Tutors can use Clove to sign up for their shifts, swap shifts to cover for each other, and get real-time queue information while they are working.

In addition, Clove can be used to get data about the usage of the tutoring center. Clove can collect information on the traffic of the tutoring centers and wait-time statistics for students, so the tutors can adjust their scheduling to meet the demand from students. Clove can also collect information on the topics students often need help with, which tutors can use to prepare for tutoring sessions, and professors can use as feedback. With this information, the tutoring centers can adjust their staffing and funding to meet the needs of the students and the abilities of the tutors.

## Data Types

Clove will mainly be responsible for maintaining information about tutoring sessions (such as topics, courses, etc.). However, the system will also be able to store media related to each session. For example, a student will be able to upload diagram photos at link them to a session at a specific tutoring center.

## Basic Usage

We anticipate the students will create an account when they come into a tutoring center for help. All they have to do is create an account and request tutoring at their tutoring center. From there, tutors will have access to the time-sensitive queue of all students who need help. The *topic* section of the tutoring session request also gives tutors a heads-up about the content before they sit down with students.

Because this system can be used from anywhere, students can reserve their place before they even leave their dorms. If a student knows that they are only available at a certain time during the week, they can use the system to set up an appointment ahead of time so that they don't have to worry about being stuck in a long queue for the whole window they have available. Instead, their queue position is assured ahead of time. This keeps wait times low so that students can get the help they need.

## Implementation Details

The system is implemented as a HTTP Restful API. Users are able to make API calls by making GET, PUT, or POST requests, depending on the type of action they would like to accomplish. They will then recieve a json dictionary as a response containing the information relevant to the action they have performed. Calls that retrieve information only are not secured, but any calls that attempt to change the state such as creating a new tutoring shift require authentication. Documentation on the calls that can be made and the responses that will be received can be found in api.markdown in the documents directory. At this time, there is no graphical frontend to the system.

