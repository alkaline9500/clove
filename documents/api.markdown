# clove API

### `POST /register`
Create new account.

#### Sample Input
```js
{
	"username" : "rsterling",
	"password" : "datpass1234",
	"email" : "rsterling@cloveapi.com",
	"first_name" : "Roger",
	"last_name" : "Sterling"
}
```

#### Sample Output
```js
{
    "username": "rsterling",
    "first_name": "Roger",
    "last_name": "Sterling",
    "email": "rsterling@cloveapi.com",
    "id": 2
}
```

### `POST /login`

Request login.

#### Sample Input
```js
{
	"username" : "rsterling",
	"password" : "datpass1234"
}
```

#### Sample Output
```js
{
    "token": "c3a5416dfe25e1688b929ad28aec7f249a0db4983cae0d50b15c3aa63f6437fa",
    "user": {
        "username": "rsterling",
        "first_name": "Roger",
        "last_name": "Sterling",
        "email": "rsterling@cloveapi.com",
        "id": 2
    }
}
```
### `POST /logout`

Request logout.

#### Sample Output
```js
{
	"message": "Successfully deleted.",
	"success": true
}
```

### `PUT /users`

Query all users. All keys are optional for filtering.

#### Sample Input

```js
{
	"username" : "rsterling"
}
```

#### Sample Output
```js
[
	{
		"username": "rsterling",
		"first_name": "Roger",
		"last_name": "Sterling",
		"email": "rsterling@cloveapi.com",
		"id": 2
	}
]
```

### `GET /users/<id>`

Find a specific user

#### Sample Output
```js
{
	"username": "rsterling",
	"first_name": "Roger",
	"last_name": "Sterling",
	"email": "rsterling@cloveapi.com",
	"id": 2
}
```

### `POST /centers`

Create a new tutoring center.

#### Sample Input
```js
{
	"name" : "Bates Tutoring Center",
	"location" : "Gosnell"
}
```

#### Sample Output

```js
{
	"id" : 1,
	"name" : "Bates Tutoring Center",
	"location" : "Gosnell"
}
```

### `PUT /centers`

Queries all tutoring centers. All keys are optional for filtering.

#### Sample Output
```js
[
    {
        "id" : 1,
        "name" : "Bates Tutoring Center",
        "location" : "Gosnell"
    },
    {
        "id" : 2,
        "name" : "Computer Science Mentoring Center",
        "location" : "Golisano"
    }
]
```

### `POST /sessions`

Creates a new session.

#### Sample Input

```js
{
	"center" : 1,
	"topic" : "Physics"
}
```

#### Sample Output
```js
{
	"center": {
		"location": "Gosnell",
		"name": "Bates Tutoring Center",
		"id": 1
	},
	"student": {
		"id": 1,
		"username": "nmanoogian",
		"last_name": "Manoogian",
		"email": "njm7461@rit.edu",
		"first_name": "Nic"
	},
	"requested_time": 1418073764,
	"media": [],
	"topic": "Physics",
	"id": 24,
	"status": 1
}
```

### `PUT /sessions`

Queries all sessions. All keys are optional for filtering.

#### Sample Output
```js
[
	{
		"center": {
			"location": "Gosnell",
			"name": "Bates Tutoring Center",
			"id": 1
		},
		"student": {
			"id": 1,
			"username": "nmanoogian",
			"last_name": "Manoogian",
			"email": "njm7461@rit.edu",
			"first_name": "Nic"
		},
		"requested_time": 1418073764,
		"media": [],
		"topic": "Physics",
		"id": 24,
		"status": 1
	}
]
```


### `GET /queue/<centerid>`

Lists the queue for a specific tutoring center.

#### Sample Output
```js
[
	{
		"center": {
			"location": "Gosnell",
			"name": "Bates Tutoring Center",
			"id": 1
		},
		"student": {
			"id": 10,
			"username": "ros1705",
			"last_name": "Stutler",
			"email": "ros1705@rit.edu",
			"first_name": "Roxanne"
		},
		"requested_time": 1418048009,
		"media": [],
		"topic": "Linear Algebra",
		"id": 2,
		"status": 1
	},
	{
		"center": {
			"location": "Gosnell",
			"name": "Bates Tutoring Center",
			"id": 1
		},
		"student": {
			"id": 8,
			"username": "anv2486",
			"last_name": "Vanantwerp",
			"email": "anv2486@rit.edu",
			"first_name": "Alica"
		},
		"requested_time": 1418048010,
		"media": [],
		"topic": "Linear Algebra",
		"id": 3,
		"status": 1
	}
]
```

### `POST /sessions/<sessionid>/begin`

Sets the `start_time` and `tutor` for a session. This moves the session from *requested* to *in progress*.

#### Sample Input

```js
{
	"tutor" : 1
}
```

#### Sample Output

```js
{
	"center": {
		"location": "Golisano",
		"name": "Computer Science Mentoring Center",
		"id": 2
	},
	"student": {
		"id": 10,
		"username": "ros1705",
		"last_name": "Stutler",
		"email": "ros1705@rit.edu",
		"first_name": "Roxanne"
	},
	"requested_time": 1418048010,
	"start_time": 1418074119,
	"media": [],
	"tutor": {
		"id": 1,
		"username": "nmanoogian",
		"last_name": "Manoogian",
		"email": "njm7461@rit.edu",
		"first_name": "Nic"
	},
	"topic": "Java",
	"id": 5,
	"status": 2
}
```

### `POST /sessions/<sessionid>/end`

Sets the `end_time` for a session. This moves the session from *in progress* to *completed*.

#### Sample Input

```js
{
	"tutor" : 1
}
```

#### Sample Output

```js
{
	"center": {
		"location": "Golisano",
		"name": "Computer Science Mentoring Center",
		"id": 2
	},
	"student": {
		"id": 10,
		"username": "ros1705",
		"last_name": "Stutler",
		"email": "ros1705@rit.edu",
		"first_name": "Roxanne"
	},
	"status": 3,
	"requested_time": 1418048010,
	"start_time": 1418074119,
	"media": [],
	"tutor": {
		"id": 1,
		"username": "nmanoogian",
		"last_name": "Manoogian",
		"email": "njm7461@rit.edu",
		"first_name": "Nic"
	},
	"topic": "Java",
	"id": 5,
	"end_time": 1418074196
}
```

### `POST /shifts`

Creates a new shift.

#### Sample Input

```js
{
	"center" : 2,
	"start_time" : 1418074019,
	"end_time" : 1418074119,
	"tutor" : 1
}
```

#### Sample Output

```js
{
	"center": {
		"name": "Computer Science Mentoring Center",
		"location": "Golisano",
		"id": 2
	},
	"end_time": 2418010606,
	"tutor": {
		"first_name": "Nic",
		"email": "njm7461@rit.edu",
		"username": "nmanoogian",
		"id": 1,
		"last_name": "Manoogian"
	},
	"id": 7,
	"start_time": 1
}
```

### `PUT /shifts`

Queries all shifts. All keys are optional for filtering.

#### Sample Input

```js
{
	"center" : 2,
}
```

#### Sample Output

```js
[
	{
		"start_time": 1,
		"center": {
			"location": "Golisano",
			"name": "Computer Science Mentoring Center",
			"id": 2
		},
		"tutor": {
			"id": 1,
			"username": "nmanoogian",
			"last_name": "Manoogian",
			"email": "njm7461@rit.edu",
			"first_name": "Nic"
		},
		"id": 1,
		"end_time": 2418010606
	}
]
```

### `GET /currentshifts/<centerid>`

Lists all current shifts for a current tutoring center.


#### Sample Output

```js
[
	{
		"start_time": 1,
		"center": {
			"location": "Golisano",
			"name": "Computer Science Mentoring Center",
			"id": 2
		},
		"tutor": {
			"id": 1,
			"username": "nmanoogian",
			"last_name": "Manoogian",
			"email": "njm7461@rit.edu",
			"first_name": "Nic"
		},
		"id": 1,
		"end_time": 2418010606
	}
]
```

### `POST /media`

Assigns a new piece of media to a session.

#### Sample Input

```js
{
	"url": "http://i.imgur.com/wyFufOe.gif",
	"session": 1
}
```
#### Sample Output

```js
{
	"url": "http://i.imgur.com/wyFufOe.gif",
	"session": 1
}
```

### `GET /sessions/<sessionid>`

Finds a specific session.

### Sample Output

```js
{
	"center": {
		"name": "Computer Science Mentoring Center",
		"id": 2,
		"location": "Golisano"
	},
	"tutor": {
		"username": "nmanoogian",
		"first_name": "Nic",
		"last_name": "Manoogian",
		"email": "njm7461@rit.edu",
		"id": 1
	},
	"end_time": 1418073632,
	"id": 1,
	"media": [
	"http://i.imgur.com/wyFufOe.gif",
	"http://i.imgur.com/wyFufOf.gif",
	"http://i.imgur.com/wyFufOh.gif"
	],
	"status": 3,
	"student": {
		"username": "cid6354",
		"first_name": "Chance",
		"last_name": "Dieterich",
		"email": "cid6354@rit.edu",
		"id": 4
	},
	"start_time": 1418073627,
	"requested_time": 1418048009,
	"topic": "Java"
}
```
