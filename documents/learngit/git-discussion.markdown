# clove API - git intro. & practices

## git crash course
Read this git intro if you haven't used git before - it does a good job of explaining things.
`http://rsb.io/talks/git/#slide1`

## git practices for clove (discussion)
I'd suggest the following development model, as it's worked well at both my coops so far (team of 3 and team of 12):

1) Choose a feature to work on

2) Create a new branch for the feature (either within the main clove repo or in a fork)

3) Develop, test, etc.

4) Create a Pull Request against the main repo with your changes

5) Other members perform code review on your pull request. (Probably at least 2 +1's to merge)

6) Code is merged, rinse and repeat

We should talk about this more as we get closer to the time we'll actually be doing development.

-nickdepinet


