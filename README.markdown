# clove

## Overview

Clove is a queuing system for tutors and students.

## API

The clove backend API is [RESTful](http://www.restapitutorial.com/lessons/whatisrest.html).

Read the clove API documentation for more information.

## Dependencies

### API

 - python
 - peewee
 - flask
 - bcrypt
 - argparse
 - requests


#### Install Using pip

```bash
pip3 install flask peewee py-bcrypt argparse requests
```

## Usage

### Running the Server

```bash
cd api/
python3 webapi.py
```

### Clearing Tokens

Periodically, old authorization tokens must be purged from the system. The following script should be executed approximately every hour for security reasons.

```bash
cd api/
./clear_expired_tokens.py
```

### Generating Sample Data

A tool for generating some sample data has been provided in this project.

```bash
cd clove_tests/
python3 populate.py
```

If the server is not running on `localhost:8888`, the following flag must be set:

```bash
python3 populate.py --url http://example.com:2100/api
```

## Directory Structure

All code for the rest api resides in the /api folder
/api - All api code
/api/webapi.py - The web api program
/api/clovedb - All models in the database
/api/clove_tests - Testing, database population script
/documents - Project Documentation
/documents/api.markdown - Api usage document, shows all endpoints and sample input/responses
/documents/learngit/git-discussion.markdown - Some git howto and practices suggestion
/documents/design - Project Design Documents
/documents/design/TableReductionSetup.markdown - Reduced tables and create SQL statements
/documents/design/cloveER.xml - XML Diagram
/documents/design/models.markdown - Clove Entity & Relationship set documentation
/documents/design/proposal.markdown - Project proposal/paper
